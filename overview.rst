.. _overview:

========
Overview
========

Features
--------

* SAML 2.0 Identity and service provider
* OpenID 1.0 and 2.0 identity provider
* Server CAS 1.0 and 2.0 using a plugin
* Standards authentication mechanisms:

    * Login/password through internal directory or LDAP
    * X509 certificate over SSL/TLS

* Protocol proxying, for instance between OpenID and SAML
* Support of LDAP v2 and v3 directories
* Support of the PAM backend
* One-time password (OATH and Google-Authenticator) using a plugin
* Identity attribute management
* Plugin system


Resources
---------

* Mailing list: http://listes.entrouvert.com/info/authentic
* Documentation: https://authentic2.readthedocs.io/
* Issues: https://dev.entrouvert.org/projects/authentic
* Sources: https://git.entrouvert.org/authentic.git and https://git.entrouvert.org/authentic2-doc.git
* Packages: https://deb.entrouvert.org/
