.. _configuration_ldap:

==================
LDAP Configuration
==================

Introduction
============

The `Authentic LDAP backend <https://git.entrouvert.org/authentic.git/tree/src/authentic2/backends/ldap_backend.py>`__ is configured by overriding the `dictionary <https://git.entrouvert.org/authentic.git/tree/src/authentic2/backends/ldap_backend.py#n531>`__ found in the sources with a `LDAP_AUTH_SETTINGS` dictionary in the Django settings file.

The

Settings
========

* **url** (required):
* **basedn** (required): the `distinguished name` used for searching LDAP
* **binddn** and **bindpw**: if both are set and no credentials were provided by the caller, they will be used as the `distinguished name` and the `password` of the LDAP `bind` function. If they are not set the `bind` function will be anonymous.
* **bindsasl**: if set and no credentials were provided by the caller, it must be a list of three elements. The first is the string that specifies the LDAP SASL mechanism (for instance 'gssapi', 'cram_md5', etc. See `the documentation for the full list <https://www.python-ldap.org/en/python-ldap-3.3.0/reference/ldap-sasl.html#classes>`__), the second is the `distinguised name` used with the `bind` function, the third is a list that will be provided verbatim to the constructor of the SASL mechanism class.
* **user_dn_template**: if set the name of the user `username` will be converted into `user_dn_template.format(username=username)`. For instance if `user_dn_template` is `geek({username})`, then the user `foo` will be transformed into `geek(foo)`, the user `bar` will be transformed into `geek(bar)` etc.
* **user_basedn**: the `distinguished name` used for searching LDAP users (defaults to **basedn** if not set)
* **user_filter**: the `LDAP filter when searching for users <https://www.python-ldap.org/en/python-ldap-3.3.0/reference/ldap-filter.html#ldap.filter.filter_format>`__ (defaults to 'uid=%s' or '(|(mail=%s)(uid=%s))' if the A2_ACCEPT_EMAIL_AUTHENTICATION setting is set)
* **sync_ldap_users_filter**: the `LDAP filter <https://www.python-ldap.org/en/python-ldap-3.3.0/reference/ldap-filter.html>`__ when searching for all users (defaults to 'uid=*')
* **group_basedn**: the `distinguished name` to use when searching for groups (defaults to **basedn**)
* **member_of_attribute** (required): the LDAP attributes of a LDAP user that are to be interpreted as containing the `distinguished name` of a group to which the user belongs to (defaults to '')
* **group_filter**: the `LDAP filter when searching for groups <https://www.python-ldap.org/en/python-ldap-3.3.0/reference/ldap-filter.html#ldap.filter.filter_format>`__ (defaults to '(&(member={user_dn})(objectClass=groupOfNames))')
* **groupsu**: the list of `groupOfNames` for which the `is_admin` flag will be set when a user becomes a member (example [u'cn=group1,o=ôrga'])
* **groupstaff**: the list of `groupOfNames` for which the `is_staff` flag will be set when a user becomes a member (example [u'cn=group1,o=ôrga'])
* **groupactive**: the list of `groupOfNames` for which the `is_active` flag will be set when a user becomes a member (example [u'cn=group1,o=ôrga'])
* **group_mapping**: a list mapping `groupOfNames` to a list of groups names. For instance [[u'cn=group2,o=ôrga', ['Group2']]] will ensure that a user that belongs to the 'cn=group2,o=ôrga' LDAP group also belongs to the Authentic group 'Group2'. The user will be removed from the Authentic groups that are not mentionned in this setting.
* **group_to_role_mapping**: a list mapping `groupOfNames` to a list of roles names. For instance [[u'cn=group2,o=ôrga', ['Role2']]] will ensure that a user that belongs to the 'cn=group2,o=ôrga' LDAP group also belongs to the Authentic role 'Role2'. The user will be removed from the Authentic roles that are not mentionned in this setting.
