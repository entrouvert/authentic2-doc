.. _development:

===========
Development
===========

Get the code and the dependencies
=================================

1. Clone the repository::

     $ git clone https://git.entrouvert.org/authentic.git

2. Install `lasso <http://lasso.entrouvert.org>`_ python packages::

     $ curl https://deb.entrouvert.org/entrouvert.gpg | sudo apt-key add -
     $ echo deb http://deb.entrouvert.org/ buster main | \
            sudo tee /etc/apt/sources.list.d/entrouvert.list
     $ sudo apt-get update
     $ sudo apt-get install python-lasso python3-lasso

3. Install dependencies::

     $ sudo apt-get install postgresql build-essential gettext sassc \
                            libldap2-dev libsasl2-dev python3-dev

Run the tests
=============

1. Setup a virtualenv::

     $ sudo apt-get install direnv
     $ echo layout python3 > authentic/.envrc
     $ direnv allow authentic
     $ cd authentic
     $ pip install tox

2. Run::

     $ JENKINS_URL=fakeurl pg_virtualenv tox

.. note::

   `tests/test_ldap.py` will fail if `apparmor_status` reports that
   `/usr/sbin/slapd` is in enforce mode.

.. note::

   Setting `JENKINS_URL` instructs tox to run `[tox:jenkins]` instead
   of the default `[tox]`.

2. Run with code coverage::

     $ pg_virtualenv tox < /dev/null
     $ firefox htmlcov/index.html

.. note::

   When running from an interactive shell `< /dev/null` unsets
   the `tty` tox sub-type and defines the `COVERAGE` variable.

Update translations
===================

They are updated upstream before the releases to help keep the
vocabulary consistent.

.. note::

   The only exception would be a trivial fix such as a typo.

The `.po` files can be updated as follows::

     $ ./update-locales.sh

Build the documentation
=======================

1. Clone the repository::

     $ git clone https://git.entrouvert.org/authentic2-doc.git

2. Setup a virtualenv::

     $ sudo apt-get install direnv
     $ echo layout python3 > authentic2-doc/.envrc
     $ direnv allow authentic2-doc
     $ cd authentic2-doc
     $ pip install sphinx

1. Build::

     $ make html

2. Display::

     $ firefox _build/html/index.html
