.. _installation:

============
Installation
============

1. Setup the repository::

    $ wget -O - https://deb.entrouvert.org/entrouvert.gpg | apt-key add -
    $ echo deb http://deb.entrouvert.org/ buster main >> /etc/apt/sources.list
    $ apt update

2. Create the database::

    $ apt install apg
    $ PASSWORD=$(apg -n 1 -M NCL -m 20)
    $ apt install postgresql
    $ su - postgres -c psql <<EOF
    CREATE ROLE authentic2 WITH LOGIN SUPERUSER PASSWORD '$PASSWORD';
    ALTER ROLE authentic2 SET client_encoding TO 'utf8';
    CREATE DATABASE authentic2;
    GRANT ALL PRIVILEGES ON DATABASE authentic2 TO authentic2;
    EOF
    $ mkdir -p /etc/authentic2/settings.d
    $ cat > /etc/authentic2/settings.d/database.py <<EOF
    DATABASES = {
       "default": {
         "ENGINE": "django.db.backends.postgresql",
         "NAME": "authentic2",
         "USER": "authentic2",
         "PASSWORD": "$PASSWORD",
         "HOST": "127.0.0.1",
         "PORT": "5432",
       }
    }
    EOF

.. warning::

   The authentic2 user is **SUPERUSER** because the authentic2
   installation process uses it to create postgres extensions. This
   privilege should be dropped if in production.

3. Install::

    $ apt install authentic2

.. note::

   The Debian GNU/Linux packages published at https://deb.entrouvert.org/ follow the `release cycle of Publik <https://dev.entrouvert.org/projects/publik/wiki/Cycle_de_mises_%C3%A0_jour>`__.

Specifying a different database
-------------------------------

This is done by modifying the DATABASES dictionary in
`/etc/authentic2/settings.d/database.py`. For example::

 DATABASES['default'] = {
   'ENGINE': 'django.db.backends.postgresql',
   'NAME': 'authentic',
   'USER': 'admindb',
   'PASSWORD': 'foobar',
   'HOST': 'db.example.com',
   'PORT': '', # empty string means default value
 }

You should refer to the Django documentation on databases settings at
http://docs.djangoproject.com/en/dev/ref/settings/#databases for all
the details.

Running Authentic 2 behind an Apache2 reverse proxy
---------------------------------------------------

Authentic 2 runs as an `uWSGI daemon
<https://uwsgi-docs.readthedocs.io/en/latest/>`__ and can be
configured as a backend for an Apache2 reverse proxy by adding
the following example to `/etc/apache2/sites-available/authentic2.conf`::

  <VirtualHost *:80>
        ServerName idp.example.com
        DocumentRoot /var/www/empty

        RedirectPermanent / https://idp.example.com

        ErrorLog ${APACHE_LOG_DIR}/idp.example.com-error.log
        CustomLog ${APACHE_LOG_DIR}/idp.example.com-access.log combined
  </Virtualhost>

  <Virtualhost *:443>
        ServerName idp.example.com
        DocumentRoot /var/www/empty
        SSLEngine On

        Alias /static /var/lib/authentic2/collectstatic
        ProxyPass /static !
        <Directory /var/lib/authentic2/collectstatic>
                Require all granted
        </Directory>

        ProxyRequests Off
        ProxyPreserveHost on
        ProxyPass / unix:/var/run/authentic2/authentic2.sock|http://127.0.0.1/
        ProxyPassReverse / unix:/var/run/authentic2/authentic2.sock|http://127.0.0.1/
        <Location />
                RequestHeader set X-Forwarded-SSL "on"
                RequestHeader set X-Forwarded-Protocol "ssl"
                RequestHeader set X-Forwarded-Proto "https"
                Require all granted
        </Location>

        ErrorLog ${APACHE_LOG_DIR}/idp.example.com-error.log
        CustomLog ${APACHE_LOG_DIR}/idp.example.com-access.log combined
  </VirtualHost>

And the enable it with::

    $ a2ensite authentic2

It also requires modules that can be enabled as follows::

    $ a2enmod proxy_http headers ssl

Quickstarts
-----------

.. toctree::
    :maxdepth: 1

    quick_oauth2_idp
    quick_saml2_idp
    quick_saml2_sp
    quick_cas_idp 
