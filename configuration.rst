.. _configuration:

=============
Configuration
=============

Configuration from files
========================

Authentic 2 can be configured by adding or overriding:

* `Django settings <https://docs.djangoproject.com/en/2.2/topics/settings/>`__
* `Authentic 2 Django settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2/settings.py>`__
* `Authentic 2 A2_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2/app_settings.py>`__
* `Authentic 2 SAML_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2/saml/app_settings.py>`__
* `Authentic 2 A2_FC_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2_auth_fc/app_settings.py>`__
* `Authentic 2 A2_AUTH_OIDC_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2_auth_oidc/app_settings.py>`__
* `Authentic 2 A2_AUTH_SAML_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2_auth_saml/app_settings.py>`__
* `Authentic 2 A2_IDP_CAS_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2_idp_cas/app_settings.py>`__
* `Authentic 2 A2_IDP_OIDC_* settings <https://git.entrouvert.org/authentic.git/tree/src/authentic2_idp_oidc/app_settings.py>`__
* :ref:`configuration_ldap`

The following files will be read, in the following order, and their
value will override the defaults found in the sources above:

* `/etc/authentic2/config.py`
* `/etc/authentic2/settings.d/*.py` (sorted in alphabetical order)

Configuration with the administration interface
===============================================

Basics
------

.. toctree::
    :maxdepth: 1

    admin_access

    administration_with_policies

SAML2
-----

.. toctree::
    :maxdepth: 1

    where_metadata

    config_saml2_sp

    config_saml2_idp

    saml2_slo

CAS
---

.. toctree::
    :maxdepth: 1

    config_cas_sp

Attributes
----------

.. toctree::
    :maxdepth: 1

    attribute_management

User interfaces and interactions
--------------------------------

.. toctree::
    :maxdepth: 1

    consent_management

Administration
==============

.. toctree::
    :maxdepth: 1

    translation

    sync-metadata_script
